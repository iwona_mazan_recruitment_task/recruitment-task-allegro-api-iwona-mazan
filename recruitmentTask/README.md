## About Project recruitmentTask

Project was created in Java programming language using the Rest-assured library.
The recruitmentTask project will test the following allegro API operations:

1. Get IDs of Allegro categories.
2. Get a category by ID.
3. Get parameters supported by a category.

---

## To run the tests with IntelliJ IDEA

1. Install IntelliJ IDEA.
2. Required: Java (project used Java 10), REST-assured library, JUnit library.
2. Clone the repository:
(https://iwona_mazan_recruitment_task@bitbucket.org/iwona_mazan_recruitment_task/recruitment-task-allegro-api-iwona-mazan.git)
3. To access the Allegro REST API a clientId and secret is required. In order to get one it is necessary to register first
(https://apps.developer.allegro.pl/new).
4. Inside the project in the resources folder there is a properties file named 'allegro.properties'. You need to set
your clientId and secret there.
5. Start the tests with the Run button.