package POJOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Categories {

    private Category[] categories;

    public Categories(Category[] categories) {
        this.categories = categories;
    }

    public Categories() {
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "categories=" + (categories == null ? null : Arrays.asList( categories )) +
                '}';
    }
}
