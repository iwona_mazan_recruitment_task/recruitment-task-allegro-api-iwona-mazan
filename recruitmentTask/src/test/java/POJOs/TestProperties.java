package POJOs;

import java.io.IOException;
import java.util.Properties;

public class TestProperties {
    private String clientId;
    private String secret;
    private String tokenUri;
    private String baseUri;
    private String contentType;

    public TestProperties(){
        try{
            Properties properties = new Properties();
            properties.load(getClass()
                    .getClassLoader().getResourceAsStream("allegro.properties"));
            clientId = properties.getProperty( "clientId" );
            secret = properties.getProperty( "secret" );
            tokenUri = properties.getProperty( "tokenUri" );
            baseUri = properties.getProperty( "baseUri" );
            contentType = properties.getProperty( "contentType" );
        }catch(IOException ex){
            // TODO: handle exception
        }
    }

    public String getClientId() {
        return clientId;
    }

    public String getSecret() {
        return secret;
    }

    public String getTokenUri() {
        return tokenUri;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public String getContentType() {
        return contentType;
    }
}
