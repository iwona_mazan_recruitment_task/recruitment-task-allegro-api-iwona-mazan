package POJOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameters {
    public Parameter[] getParameters() {
        return parameters;
    }

    public void setParameters(Parameter[] parameters) {
        this.parameters = parameters;
    }

    private Parameter[] parameters;

    @Override
    public String toString() {
        return "Parameters{" +
                "parameters=" + (parameters == null ? null : Arrays.asList( parameters )) +
                '}';
    }
}
