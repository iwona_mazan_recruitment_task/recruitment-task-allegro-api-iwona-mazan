package EndpointsConfig;

import POJOs.Token;

import static io.restassured.RestAssured.given;

public class TokenEndpoint {

    public static String getAccessToken(String username, String password, String tokenUri) {
        return given()
            .auth().preemptive().basic(username,  password)
        .expect()
            .statusCode(200)
        .when()
            .post(tokenUri)
        .then()
            .extract()
            .body().as(Token.class).getAccessToken();
    }
}
