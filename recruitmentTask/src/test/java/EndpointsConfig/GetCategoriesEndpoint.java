package EndpointsConfig;

import POJOs.Categories;
import POJOs.Category;

public class GetCategoriesEndpoint extends BasicEndpointConfiguration {

    private final String ENDPOINT_ID_CATEGORIES = "categories";
    private final String QUERY_PARAM_PARENT_ID = "parent.id";

    public GetCategoriesEndpoint(String basePath){
        super(basePath);
    }

    public Category[] getCategories(){
        lastResponse = this.prep().get(ENDPOINT_ID_CATEGORIES);
        return lastResponse.as(Categories.class).getCategories();
    }

    public Category[] getCategories(String parentId){
        lastResponse = this.prep().queryParam(QUERY_PARAM_PARENT_ID, parentId)
                .get(ENDPOINT_ID_CATEGORIES);
        return lastResponse.as(Categories.class).getCategories();
    }
}
