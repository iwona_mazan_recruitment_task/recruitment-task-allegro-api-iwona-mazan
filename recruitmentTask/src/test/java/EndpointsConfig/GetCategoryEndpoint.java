package EndpointsConfig;

import POJOs.Category;

import static io.restassured.RestAssured.get;

public class GetCategoryEndpoint extends BasicEndpointConfiguration {

    private final String ENDPOINT_CATEGORY_BY_ID = "categories/";

    public GetCategoryEndpoint(String basePath){
        super(basePath);
    }

    public Category getCategory(String id){
        lastResponse = this.prep().get(ENDPOINT_CATEGORY_BY_ID + id);
        return lastResponse.as( Category.class);
    }
}
