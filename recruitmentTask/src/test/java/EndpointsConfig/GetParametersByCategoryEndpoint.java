package EndpointsConfig;

import POJOs.Parameter;
import POJOs.Parameters;

public class GetParametersByCategoryEndpoint extends BasicEndpointConfiguration {

    private final String ENDPOINT_CATEGORY_PARAMETERS = "categories/";
    private final String ENDPOINT_PARAMETERS = "/parameters";

    public GetParametersByCategoryEndpoint(String basePath){
        super(basePath);
    }

    public Parameter[] getParametersByCategory(String id){
        lastResponse = this.prep().get(ENDPOINT_CATEGORY_PARAMETERS + id + ENDPOINT_PARAMETERS);
        return lastResponse.as( Parameters.class).getParameters();
    }

}
