package EndpointsConfig;

import Helpers.Singleton;
import POJOs.TestProperties;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.oauth2;

public abstract class BasicEndpointConfiguration {

    private static RequestSpecification requestSpec;

    protected Response lastResponse;

    public int getLastStatusCode(){
        return lastResponse.statusCode();
    }

    public Response getLastResponse() {
        return lastResponse;
    }

    public void setLastResponse(Response lastResponse) {
        this.lastResponse = lastResponse;
    }

    protected RequestSpecification prep(){
        return given().spec(this.requestSpec);
    }

    public BasicEndpointConfiguration(String basePath){
        TestProperties properties = Singleton.getInstance().getTestProperties();
        RestAssured.baseURI = properties.getBaseUri();
        RestAssured.basePath = basePath;
        RestAssured.authentication = oauth2(Singleton.getInstance().getToken());
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("Accept", properties.getContentType());
        requestSpec = builder.build();
    }
}
