package Helpers;

import EndpointsConfig.TokenEndpoint;
import POJOs.TestProperties;

public class EndpointHelper {
    public static String getAuthToken(TestProperties properties){
        return TokenEndpoint.getAccessToken(properties.getClientId(),
                properties.getSecret(),
                properties.getTokenUri());
    }

    public static boolean validateParameter(String parameter){
        if(parameter != null && parameter.trim().length() > 0)
            return true;
        else
            throw new IllegalArgumentException();
    }
}
