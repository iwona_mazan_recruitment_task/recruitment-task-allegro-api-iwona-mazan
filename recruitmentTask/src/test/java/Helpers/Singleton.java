package Helpers;

import POJOs.TestProperties;

public class Singleton {

    private Singleton() {
        if (Holder.INSTANCE != null) {
            throw new IllegalStateException("Helpers.Singleton already constructed");
        }

        properties = new TestProperties();
        token = EndpointHelper.getAuthToken(properties);
    }

    public String getToken() {
        return token;
    }

    public TestProperties getTestProperties() {
        return properties;
    }

    private String token;
    private TestProperties properties;

    public static Singleton getInstance() {
        return Holder.INSTANCE;
    }

    private static class Holder {
        private static final Singleton INSTANCE = new Singleton();
    }
}
