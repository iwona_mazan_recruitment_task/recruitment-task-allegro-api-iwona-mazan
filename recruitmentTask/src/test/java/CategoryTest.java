import EndpointsConfig.GetCategoriesEndpoint;
import EndpointsConfig.GetCategoryEndpoint;
import EndpointsConfig.GetParametersByCategoryEndpoint;
import POJOs.Category;
import POJOs.Parameter;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CategoryTest extends BaseTest {

    private final String basePath = "sale/";

    @Test
    public void getCategories(){
        GetCategoriesEndpoint getCategoriesEndpoint = new GetCategoriesEndpoint( basePath );
        Category[] categories = getCategoriesEndpoint.getCategories();
        Assertions.assertEquals(HttpStatus.SC_OK, getCategoriesEndpoint.getLastStatusCode());
        Assertions.assertTrue(categories.length > 1);
        Assertions.assertEquals("11763", categories[1].getId());
        Assertions.assertEquals("Dziecko", categories[1].getName());
    }

    @Test
    public void getCategoriesByParentId(){
        GetCategoriesEndpoint getCategoriesEndpoint = new GetCategoriesEndpoint( basePath );
        Category[] categories = getCategoriesEndpoint.getCategories("1536");
        Assertions.assertEquals(HttpStatus.SC_OK, getCategoriesEndpoint.getLastStatusCode());
        Assertions.assertTrue(categories.length > 0);
        Assertions.assertEquals("147642", categories[0].getId());
        Assertions.assertEquals("Agregaty prądotwórcze", categories[0].getName());
    }

    @Test
    public void getCategoriesById(){
        GetCategoryEndpoint getCategoryEndpoint = new GetCategoryEndpoint( basePath );
        Category category = getCategoryEndpoint.getCategory("12");
        Assertions.assertEquals( HttpStatus.SC_OK, getCategoryEndpoint.getLastStatusCode());
        Assertions.assertEquals("12", category.getId());
        Assertions.assertEquals("Pozostałe", category.getName());
    }

    @Test
    public void getParametersByCategory(){
        GetParametersByCategoryEndpoint getParametersByCategoryEndpoint = new GetParametersByCategoryEndpoint( basePath );
        Parameter[] parametersByCategory = getParametersByCategoryEndpoint.getParametersByCategory("15999");
        Assertions.assertEquals(HttpStatus.SC_OK, getParametersByCategoryEndpoint.getLastStatusCode());
        Assertions.assertEquals("2986", parametersByCategory[0].getId());
        Assertions.assertEquals("Płeć", parametersByCategory[0].getName());
        Assertions.assertEquals( "dictionary", parametersByCategory[0].getType());
    }

    @Test
    public void getCategoriesByParentIdFail404(){
        GetCategoriesEndpoint getCategoriesEndpoint = new GetCategoriesEndpoint( basePath );
        getCategoriesEndpoint.getCategories("Nie istniejacy parent id");
        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, getCategoriesEndpoint.getLastStatusCode());
    }

    @Test
    public void getCategoriesByIdFailed404(){
        GetCategoryEndpoint getCategoryEndpoint = new GetCategoryEndpoint( basePath );
        getCategoryEndpoint.getCategory("Nie istniejace id");
        Assertions.assertEquals( HttpStatus.SC_NOT_FOUND, getCategoryEndpoint.getLastStatusCode());
    }

    @Test
    public void getParametersByCategoryFailed404(){
        GetParametersByCategoryEndpoint getParametersByCategoryEndpoint = new GetParametersByCategoryEndpoint( basePath );
        getParametersByCategoryEndpoint.getParametersByCategory("Nie istniejace id");
        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, getParametersByCategoryEndpoint.getLastStatusCode());
    }

    @Test
    public void e2e(){
        GetCategoriesEndpoint getCategoriesEndpoint = new GetCategoriesEndpoint( basePath );
        Category[] categories = getCategoriesEndpoint.getCategories();
        Assertions.assertEquals(HttpStatus.SC_OK, getCategoriesEndpoint.getLastStatusCode());
        Assertions.assertTrue(categories.length > 0);
        Assertions.assertEquals("5", categories[0].getId());

        GetCategoryEndpoint getCategoryEndpoint = new GetCategoryEndpoint( basePath );
        Category category = getCategoryEndpoint.getCategory(categories[0].getId());
        Assertions.assertEquals( HttpStatus.SC_OK, getCategoryEndpoint.getLastStatusCode());
        Assertions.assertEquals(categories[0].getId(), category.getId());
        Assertions.assertEquals(categories[0].getName(), category.getName());

        GetParametersByCategoryEndpoint getParametersByCategoryEndpoint = new GetParametersByCategoryEndpoint( basePath );
        Parameter[] parametersByCategory = getParametersByCategoryEndpoint.getParametersByCategory(categories[0].getId());
        Assertions.assertEquals(HttpStatus.SC_OK, getParametersByCategoryEndpoint.getLastStatusCode());
        Assertions.assertTrue(parametersByCategory.length > 0);
        Assertions.assertEquals("11323", parametersByCategory[0].getId());
        Assertions.assertEquals("Stan", parametersByCategory[0].getName());
        Assertions.assertEquals( "dictionary", parametersByCategory[0].getType());
    }
}
